<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-sm-12">
                <a class="navbar-brand" href="#"><img src="./images/logo_destake_persianas.png" alt="Destake Persianas"></a>
            </div>
            <div class="col-lg-3 col-sm-4">
                <div class="local">
                    <h3><span>Onde Estamos</span></h3>
                    <p>R. São Luís, 725 - loja 01 - Santana, Porto Alegre - RS, 90620-170 </p>
                </div>
            </div>
            <div class="col-lg-3 col-sm-4">
                <div class="callus">
                    <h3><span>Ligue para gente</span></h3>
                    <p>(51) 3241-5356</p>
                    <p>(51) 3241-5356</p>
                </div>
            </div>
            <div class="col-lg-3 col-sm-4">
                <h3><span>Redes Sociais</span></h3>
                <div class="social">
                    <p class="facebook"><span></span></p>
                    <p class="instagram"><span></span></p>
                    <p class="youtube"><span></span></p>
                </div>
            </div>
        </div>
        <div class="conditions">
            Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo
            inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque
            porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.
        </div>
    </div>
    <div class="copyright">
        <div class="container">
            <div class="row">
                Copyright 2018 © Destake Persianas e Cortinas - Todos os direitos reservados.

                <img src="./images/logo-web2lead.png">

            </div>
        </div>
    </div>
</footer>