<header>
    <div class="contentHeader">
        <button class="button whatsappbt">Chamar no WhatsApp</button>
        <h1>Ligue agora! 51 9999.9999</h1>
    </div>
</header>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
        <div class="row">

            <a class="navbar-brand" href="index.php"><img src="./images/logo_destake_persianas.png" alt="Destake Persianas"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="blog-inter.php">Cortinas e Persianas</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="works.php">Manutenção de Cortinas</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="about.php">A Destake</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="blog-list.php">Blog</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="contato.php">Contato</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>