<?php include 'header-destake.php'; ?>

<body>
    <?php include 'nav-destake.php'; ?>
    <section>
        <div class="banner-about">
            <div class="container">
                <div class="row">
                    <div class="boxFeature">
                        <h2>SOBRE A DESTAKE</h2>
                        <h1>A Destake trabalha oferece para você o que há de melhor!</h1>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="contact">
            <div class="container feature-img-title-text break">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="row">
                            <div class="col-lg-3">
                                <img src="./images/step-icon.png" class="img-fluid" alt="passo 1">
                            </div>
                            <div class="col-lg-9">
                                <h4><span>Montamos
                                        seu Projeto!</span></h4>
                                <p>Oferecemos ao cliente uma solução no projeto de Cortinas e Persianas completa e com máximo cuidado e agilidade.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="row">
                            <div class="col-lg-3">
                                <img src="./images/step-icon.png" class="img-fluid" alt="passo 1">
                            </div>
                            <div class="col-ld-9">
                                <h4><span>Profissionais
                                        Decoradores</span></h4>
                                <p>Com profundo conhecimento sobre Cortinas e Persianas Vs Ambientes, nossa equipe qualificada nos diferencia de todo o mercado viabilizando adequar a melhor solução ao seu orçamento. </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="row">
                            <div class="col-lg-3">
                                <img src="./images/step-icon.png" class="img-fluid" alt="passo 1">
                            </div>
                            <div class="col-lg-9">
                                <h4><span>Time de
                                        instaladores </span></h4>
                                <p>Prezando sempre por uma excelente montagem e melhor finalização de seu projeto de Cortinas e Persianas, selecionamos de forma técnica nosso time de instaladores.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-12 content-text">
                    <p>A Destake Persianas trabalha oferecendo para você o que há de melhor em <span>Cortinas e Persianas</span></p>
                    <p>Para a <span>Destake Persianas</span>, atuar no segmento de <span>Persianas e Cortinas em Porto Alegre e região metropolitana</span> é buscar diariamente soluções voltadas para proteção solar, privacidade e decoração de ambientes. É também aliar conforto e bem estar de seus clientes com a total satisfação de seus parceiros comerciais e profissionais da área.</p>

                    <p>Por isso, investimos continuamente na evolução dos materiais aplicados, assim como no aprimoramento técnico de nossos profissionais. Tudo para estar sempre à frente de seu tempo, visando superar as expectativas do mercado e dos mais exigentes gostos e estilos.</p>

                    <p>A <span>Destake Persianas</span> oferece aos seus clientes e parceiros uma linha de produtos que prima pela qualidade, estética, inovação e praticidade. São diversos modelos de <span>Cortinas e Persianas</span>, desenvolvidos para proporcionar a ambientes residenciais e comerciais em <span>Porto Alegre e região</span> o que há de mais moderno no controle da luminosidade, levando a nossos clientes o bem-estar, a privacidade e o conforto que tanto merecem.</p>
                    <p style="margin-top: 40px;"><img src="./images/logo3d.png" class="img-fluid" ></p>
                </div>
            </div>
        </div>

    </section>
    <section>
        <div class="contact-feature">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-lg-7">
                        <div class="text">
                            <h3>A Destake possui vários canais de comunicação</h3>

                            <p>Envie um e-mail para contato@destakepersianas.com.br ou chame Whatsapp</p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-lg-5">
                        <button class="button whatsappbt">Chamar no WhatsApp</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php include 'footer-destake.php'; ?>
</body>

</html>