<?php include 'header-destake.php'; ?>

<body>
    <?php include 'nav-destake.php'; ?>
    <section class="modal-form">
        <div class="modal-title">
            <h4>Ligue agora! 555555555</h4>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12">

                    <div class="modal-header">
                        <h4>Fale COnosco</h4>
                        <h4><span>Agora mesmo!</span></h4>
                    </div>
                    <form action="" method="post" accept-charset="utf-8">
                        <div class="form-row">
                            <div class="form-group input-group">
                                <span id="error_firstname" class="error"></span>
                                <span class="has-float-label">
                                    <input type="text" name="firstname" value="" class="form-control" id="firstname" placeholder="Nome" required="required">
                                    <label for="firstname">Nome <span class="required">*</span></label>
                                </span>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group input-group">
                                <span id="error_emailid" class="error"></span>
                                <span class="has-float-label">
                                    <input type="email" name="emailid" value="" class="form-control" id="emailid" placeholder="Email ID" required="required">
                                    <label for="emailid">Email <span class="required">*</span></label>
                                </span>
                            </div>
                            <div class="form-group input-group">
                                <span id="error_phone" class="error"></span>
                                <span class="has-float-label">
                                    <input type="text" name="phone" value="" class="form-control" id="phone" placeholder="Telefone" required="required" pattern="^(?!(0))\d{3}[\-]\d{3}[\-]\d{4}$">
                                    <label for="phone">Telefone <span class="required">*</span> <span class="instruction"></span></label>
                                </span>
                            </div>
                        </div>
                        <div class="form-group input-group">
                            <span id="error_comments" class="error"></span>
                            <span class="has-float-label">
                                <textarea name="comments" class="form-control" id="comments" placeholder="Mensagem" required="required"></textarea>
                                <label for="comments">Mensagem<span class="required">*</span></label>
                            </span>
                        </div>
                        <input type="submit" name="btn_contactus" value="Submit" class="btn allbt" id="btn_contactus" title="Submit">
                    </form>
                </div>
            </div>
        </div>


    </section>
    <section>
        <div class="banner">
            <div class="container">
                <div class="row">
                    <div class="boxFeature">
                        <h3>Bem vindo a Destake Persianas!</h3>
                        <h2>A mais alta qualidade em Cortinas e Persianas.</h2>
                        <h1>Temos Cortinas e Persianas para Residência ou Escritório. Dê um toque de Destake especial para o seu ambiente!</h1>
                    </div>
                    <div class="buttonBanner"><button class="button allbt">Solicitar Cotação</button></div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="invite">
            <h3>Ajudamos o seu ambiente a obter <span>aparência perfeita</span> com Cortinas e Persianas que combinam com o <span>seu estilo e decoração!</span></h3>

            <button class="button allbt">Agendar visita grátis em casa</button>
        </div>
    </section>
    <section>
        <div class="depositions">
            <h3>Confira o depoimento de quem <span>acreditou</span> na Destake Cortinas
                para fazer o <span>seu projeto de Cortinas e Persianas</span></h3>
            <div class="d-flex row-testimony">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="testimony">
                                <img src="./images/tv.png" class="img-fluid" alt="Depoimentos">
                                <h6>Nome da pessoa</h6>
                                <p>Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor
                                </p>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="testimony">
                                <img src="./images/tv.png" class="img-fluid" alt="Depoimentos">
                                <h6>Nome da pessoa</h6>
                                <p>Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="howWork">
            <h3>Entenda <span>funciona o processo</span></h3>
            <div class="container feature-img-title-text">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="row">
                            <div class="col-lg-3">
                                <img src="./images/step-icon.png" class="img-fluid" alt="passo 1">
                            </div>
                            <div class="col-lg-9">
                                <h4><span>Onde estamos</span></h4>
                                <p>Rua São Luis 725 loja 01
                                    <br>Bairro: Santana Porto Alegre - RS</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="row">
                            <div class="col-lg-3">
                                <img src="./images/step-icon.png" class="img-fluid" alt="passo 1">
                            </div>
                            <div class="col-lg-9">
                                <h4><span>Onde estamos</span></h4>
                                <p>Rua São Luis 725 loja 01
                                    <br>Bairro: Santana Porto Alegre - RS</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="row">
                            <div class="col-lg-3">
                                <img src="./images/step-icon.png" class="img-fluid" alt="passo 1">
                            </div>
                            <div class="col-lg-9">
                                <h4><span>Onde estamos</span></h4>
                                <p>Rua São Luis 725 loja 01
                                    <br>Bairro: Santana Porto Alegre - RS</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <button class="button allbt">Solicite um contato telefônico</button>
        </div>
    </section>
    <section class="destake-room">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="box">
                        <button class="button allbt float">Cortinas e Persianas</button>
                        <img src="./images/foto1.png" class="img-fluid h-img-l">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="box">
                        <button class="button allbt float">Papel de parede</button>
                        <img src="./images/foto2.png" class="img-fluid h-img">
                    </div>
                    <div class="box">
                        <button class="button allbt float">Almofadas</button>
                        <img src="./images/foto3.png" class="img-fluid h-img">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="destake-persianas">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 flex-center">
                        <div class="box">
                            <h6>Sobre a <br><span>Destake Persianas</span></h6>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        </div>
                    </div>
                    <div class="col-sm-6 padding-box">
                        <div class="box">
                            <img src="./images/foto4.png" class="img-fluid h-img">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="news">
            <h3>Novidades <span>Destake</span></h3>
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="box">
                            <img src="./images/familia.png" class="img-fluid">
                            <p class="date-post"><span>OUTUBRO 3, 2019</span> por Destake</p>
                            <h4>EGESTAS VOLUTE NEMO THE IPSAM SODALES THE TURPIS</h4>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make
                                a type specimen book.</p>

                            <button class="button allbt">Leia Mais</button>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="box">
                            <img src="./images/familia.png" class="img-fluid">
                            <p class="date-post"><span>OUTUBRO 3, 2019</span> por Destake</p>
                            <h4>EGESTAS VOLUTE NEMO THE IPSAM SODALES THE TURPIS</h4>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make
                                a type specimen book.</p>

                            <button class="button allbt">Leia Mais</button>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="box">
                            <img src="./images/familia.png" class="img-fluid">
                            <p class="date-post"><span>OUTUBRO 3, 2019</span> por Destake</p>
                            <h4>EGESTAS VOLUTE NEMO THE IPSAM SODALES THE TURPIS</h4>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make
                                a type specimen book.</p>

                            <button class="button allbt">Leia Mais</button>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="box">
                            <img src="./images/familia.png" class="img-fluid">
                            <p class="date-post"><span>OUTUBRO 3, 2019</span> por Destake</p>
                            <h4>EGESTAS VOLUTE NEMO THE IPSAM SODALES THE TURPIS</h4>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make
                                a type specimen book.</p>

                            <button class="button allbt">Leia Mais</button>
                        </div>
                    </div>
                </div>
            </div>
    </section>
    <?php include 'footer-destake.php'; ?>
</body>

</html>