<?php include 'header-destake.php'; ?>

<body>
    <?php include 'nav-destake.php'; ?>
    <section>
        <div class="banner">
            <div class="container">
                <div class="row">
                    <div class="boxFeature">
                        <h3>Bem vindo a Destake Persianas!</h3>
                        <h2>A mais alta qualidade em Cortinas e Persianas.</h2>
                        <h1>Temos Cortinas e Persianas para Residência ou Escritório. Dê um toque de Destake especial para o seu ambiente!</h1>
                    </div>
                    <div class="buttonBanner"><button class="button allbt">Solicitar Cotação</button></div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="contact">
            <div class="container feature-img-title-text">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="row">
                            <div class="col-lg-3">
                                <img src="./images/step-icon.png" class="img-fluid" alt="passo 1">
                            </div>
                            <div class="col-lg-9">
                                <h4><span>Onde estamos</span></h4>
                                <p>Rua São Luis 725 loja 01
                                    <br>Bairro: Santana Porto Alegre - RS</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="row">
                            <div class="col-lg-3">
                                <img src="./images/step-icon.png" class="img-fluid" alt="passo 1">
                            </div>
                            <div class="col-lg-9">
                                <h4><span>Onde estamos</span></h4>
                                <p>51 3241.5356 | 
                                    51 3407.7725<br>
                                    51 98421.6614</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="row">
                            <div class="col-lg-3">
                                <img src="./images/step-icon.png" class="img-fluid" alt="passo 1">
                            </div>
                            <div class="col-lg-9">
                                <h4><span>Email</span></h4>
                                <p>contato@destakepersianas.com.br</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="contact-feature">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-lg-7">
                        <div class="text">
                            <h3>A Destake possui vários canais de comunicação</h3>

                            <p>Envie um e-mail para contato@destakepersianas.com.br ou chame Whatsapp</p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-lg-5">
                        <button class="button whatsappbt">Chamar no WhatsApp</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php include 'footer-destake.php'; ?>
</body>

</html>