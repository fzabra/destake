<?php include 'header-destake.php'; ?>

<body>
    <?php include 'nav-destake.php'; ?>

    <section class="works">
        <div class="container">
            <div class="row">
                <div class="col-12 content-text">
                    <h1 class="big-title">Automação de Cortinas e Persianas
                        <span>Instalação e Manutenção</span></h1>
                    <p style="text-align: center;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    <p style="text-align: center;"><button class="button allbt">Agendar visita grátis em casa</button></p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6"><img src="./images/foto4.png" class="img-fluid" alt=""></div>
                <div class="col-sm-6">
                    <h1 class="m-title">Lorem ipsum dolor sit amet</h1>
                    <h2 class="m-sub-title">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                    </p>
                    <p> <button class="button whatsappbt">Chamar no WhatsApp</button></p>
                </div>
            </div>
        </div>
        <div class="container feature-img-title-text">
            <div class="row">
                <div class="col-sm-4">
                    <div class="row">
                        <div class="col-sm-12 col-lg-3">
                            <img src="./images/tools.png" class="img-fluid" alt="passo 1">
                        </div>
                        <div class="col-sm-12 col-lg-9">
                            <h4><span>Lorem ipsum
                                    dolor sit amet</span></h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="row">
                        <div class="col-sm-12 col-lg-3">
                            <img src="./images/tools.png" class="img-fluid" alt="passo 1">
                        </div>
                        <div class="col-sm-12 col-lg-9">
                            <h4><span>Lorem ipsum
                                    dolor sit amet</span></h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="row">
                        <div class="col-sm-12 col-lg-3">
                            <img src="./images/tools.png" class="img-fluid" alt="passo 1">
                        </div>
                        <div class="col-sm-12 col-lg-9">
                            <h4><span>Lorem ipsum
                                    dolor sit amet</span></h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row space-box">
                <div class="col-sm-4 box-more">
                    <h4><span>Tem mais perguntas?</span></h4>
                    <h2 class="s-sub-title">Quer saber mais sobre nossa Automação de Cortinas e Persianas?</h2>
                    <p class="s-sub-title ligth">Tenha uma consultoria e visita gratuita no seu abiente para entender melhor o funcionamento e possibilidades do projeto.
                        Garantimos que sairás da visita entendendo todo o potencial de seu ambiente!
                    </p>
                </div>
                <div class="col-sm-8">
                    <div class="row  space-box">
                        <div class="col-sm-3 d-flex align-items-center justify-content-center flex-column">
                            <img src="./images/skill.png" class="img-fluid" alt="">
                            <h4 class="center"><span>Tem mais perguntas?</span></h4>
                            <p class="center">Quer saber mais sobre nossa Automação de Cortinas e Persianas?</p>
                        </div>
                        <div class="col-sm-3 d-flex align-items-center justify-content-center flex-column">
                            <img src="./images/skill.png" class="img-fluid" alt="">
                            <h4 class="center"><span>Tem mais perguntas?</span></h4>
                            <p class="center">Quer saber mais sobre nossa Automação de Cortinas e Persianas?</p>
                        </div>
                        <div class="col-sm-3 d-flex align-items-center justify-content-center flex-column">
                            <img src="./images/skill.png" class="img-fluid" alt="">
                            <h4 class="center"><span>Tem mais perguntas?</span></h4>
                            <p class="center">Quer saber mais sobre nossa Automação de Cortinas e Persianas?</p>
                        </div>
                        <div class="col-sm-3 d-flex align-items-center justify-content-center flex-column">
                            <img src="./images/skill.png" class="img-fluid" alt="">
                            <h4 class="center"><span>Tem mais perguntas?</span></h4>
                            <p class="center">Quer saber mais sobre nossa Automação de Cortinas e Persianas?</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php include 'footer-destake.php'; ?>
</body>

</html>